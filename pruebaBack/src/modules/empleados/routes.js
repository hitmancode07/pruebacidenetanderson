const express = require("express");
const router = express.Router();
const respuesta = require("../../red/response");
const controller = require("./index");

//obtener datos empleados
router.get("/", async (req, res) => {
  try {
    const items = await controller.todosEmpleados();
    respuesta.success(req, res, items, 200);
  } catch (err) {
    respuesta.error(req, res, err, 200);
  }
});

//obtener dato de un solo empleado
router.get("/:id", async (req, res) => {
  try {
    const items = await controller.empleados(req.params.id);
    respuesta.success(req, res, items, 200);
  } catch (err) {
    respuesta.error(req, res, err, 500);
  }
});

//agregar y actualizar datos empleados
router.post("/", async (req, res) => {
  try {
    const items = await controller.agregarEmpleado(req.body);
    if (req.body.id == 0) {
      mensaje = "Empleado guardado con exito";
    } else {
      mensaje = "Empleado actualizado con exito";
    }
    respuesta.success(req, res, mensaje, 201);
  } catch (err) {
    respuesta.error(req, res, err, 500);
  }
});


//eliminar datos del empleado
router.put("/", async (req, res) => {
  try {
    const items = await controller.eliminarEmpleados(req.body);
    respuesta.success(req, res, "Item eliminado satisfactoriamente", 200);
  } catch (err) {
    respuesta.error(req, res, err, 500);
  }
});

module.exports = router;
