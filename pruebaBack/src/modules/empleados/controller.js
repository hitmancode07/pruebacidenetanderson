//aca realizaremos las consultas a nuestra base de datos
const TABLA = "empleados";

module.exports = (dbInyectada) => {
    
    // si la base de datos tiene problemas se pueda requerir desde la db de mysql
    let db = dbInyectada;

    if(!db){
        db = require("../../db/mysql");
    }

  function todosEmpleados() {
    return db.todosEmpleados(TABLA);
  }

  function empleados(id) {
    return db.empleados(TABLA, id);
  }

  function eliminarEmpleados(body) {
    return db.eliminarEmpleados(TABLA, body);
  }

  function agregarEmpleado(body) {
    return db.agregarEmpleado(TABLA, body);
  }
  return {
    todosEmpleados,
    empleados,
    eliminarEmpleados,
    agregarEmpleado,
  };
};
