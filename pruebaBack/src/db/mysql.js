const mysql = require("mysql");
const config = require("../config");

//configuracion de la base de datos
const dbconfig = {
  host: config.mysql.host,
  user: config.mysql.user,
  password: config.mysql.password,
  database: config.mysql.database,
};

// conexion con la base de datos
let conexion;
function conection() {
  conexion = mysql.createConnection(dbconfig);

  conexion.connect((err) => {
    if (err) {
      console.log("[db err]", err);
      setTimeout(conection, 200);
    } else {
      console.log("DB Conectada!!!!");
    }
  });

  conexion.on("error", (err) => {
    console.log("[db err]", err);
    if (err.code === "PROTOCOL_CONNECTION_LOST") {
      conection();
    } else {
      throw err;
    }
  });
}
conection();

//obtener datos empleados
function todosEmpleados(tabla) {
  return new Promise((resolve, reject) => {
    conexion.query(`SELECT * FROM ${tabla}`, (error, result) => {
      return error ? reject(error) : resolve(result);
    });
  });
}

//obtener dato de un solo empleado
function empleados(tabla, id) {
  return new Promise((resolve, reject) => {
    conexion.query(
      `SELECT * FROM ${tabla} WHERE id = ${id}`,
      (error, result) => {
        return error ? reject(error) : resolve(result);
      }
    );
  });
}

//eliminar datos empleados
function eliminarEmpleados(tabla, data) {
  return new Promise((resolve, reject) => {
    conexion.query(
      `DELETE FROM ${tabla} WHERE id = ?`,
      data.id,
      (error, result) => {
        return error ? reject(error) : resolve(result);
      }
    );
  });
}

//agregar y actualizar datos empleados
function insertar(tabla, data) {
  return new Promise((resolve, reject) => {
    conexion.query(`INSERT INTO ${tabla} SET ?`, data, (error, result) => {
      return error ? reject(error) : resolve(result);
    });
  });
}

function actualizar(tabla, data) {
  return new Promise((resolve, reject) => {
    conexion.query(`UPDATE ${tabla} SET ? WHERE id = ?`, [data, data.id], (error, result) => {
      return error ? reject(error) : resolve(result);
    });
  });
}

function agregarEmpleado(tabla, data) {
  if (data && data.id == 0) {
    return insertar(tabla, data);
  } else {
    return actualizar(tabla, data);
  }
}

module.exports = {
  todosEmpleados,
  empleados,
  agregarEmpleado,
  eliminarEmpleados,
};
