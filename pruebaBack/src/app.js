const express = require('express');
const config = require('./config');
const morgan = require('morgan');
const empleados = require('./modules/empleados/routes');
const cors = require('cors');
const app = express();

//middleware
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


//configuracion del servidor 
app.set('port', config.app.port);


//rutas del sistema
app.use('/api/empleados', empleados)


module.exports = app;