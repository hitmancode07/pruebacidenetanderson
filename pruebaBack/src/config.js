//variables globales del sistema
require('dotenv').config();

module.exports = {
   app: {
     port: process.env.PORT || 4000,
   },

   //configuracion de la base de datos
   mysql: {
    host: process.env.MYSQL_HOST || 'localhost',
    user: process.env.MYSQL_USER || 'root',
    pass: process.env.MYSQL_PASSWORD || '',
    database: process.env.MYSQL_DB || 'prueba'
   }
}