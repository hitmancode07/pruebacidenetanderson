-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-02-2023 a las 23:50:37
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `primer_nombre` varchar(20) NOT NULL,
  `segundo_nombre` varchar(50) NOT NULL,
  `primer_apellido` varchar(20) NOT NULL,
  `segundo_apellido` varchar(20) NOT NULL,
  `pais` varchar(20) NOT NULL,
  `tipo_identificacion` varchar(30) NOT NULL,
  `numero_identificacion` varchar(20) NOT NULL,
  `email` varchar(300) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `area` varchar(30) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `fecha_registro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `pais`, `tipo_identificacion`, `numero_identificacion`, `email`, `fecha_ingreso`, `area`, `estado`, `fecha_registro`) VALUES
(5, 'CARLOS', 'JAVIER', 'ESCOBAR', 'PARRA', 'Estados Unidos', 'Cedula De Ciudadania', '8433893', 'anderson.escobar@cidenet.com.co', '2023-02-02', 'Operacion', 'Activo', '2023-02-23 20:10:03'),
(6, 'MIRIAM', 'MARIA', 'ESTRADA', 'ESTRADA', 'Colombia', 'Cedula De Ciudadania', '321654987', 'miriam.maria@cidenet.com.co', '2023-01-05', 'Servicios Varios', 'Activo', '2023-02-23 20:10:03'),
(7, 'ANDRES', 'MAURICIO', 'CARDENAS', 'PEREZ', 'Estados Unidos', 'Cedula De Extranjeria', 'eu154263', 'andres.cardenas@cidenet.com.co', '2023-02-22', 'Infraestructura', 'Activo', '2023-02-24 01:10:03'),
(8, 'MARIA', 'CAMILA', 'RODRIGUEZ', 'PALACIO', 'Colombia', 'Pasaporte', 'P201598', 'maria.rodriguez@cidenet.com.co', '2023-02-01', 'Compras', 'Activo', '2023-02-24 01:10:03'),
(9, 'GIOVANNY', 'ALBERTO', 'PARRA', 'PARRA', 'Estados Unidos', 'Cedula De Extranjeria', 'CE1122335', 'giovanny.parra@cidenet.com.co', '2023-01-25', 'Talento Humano', 'Activo', '2023-02-24 01:10:03'),
(10, 'JUAN', 'CAMILO', 'CORTEZ', 'SIERRA', 'Colombia', 'Cedula de ciudadania', '12566998', 'juan.cortez@cidenet.com.co', '2023-02-03', 'Talento Humano', 'Activo', '2023-02-18 15:55:00'),
(11, 'LENSY', 'YURANE', 'CASTANO', 'CASTANO', 'Colombia', 'Cedula de ciudadania', '556623211', 'lensy.castano@cidenet.com.co', '2023-02-07', 'Infraestructura', 'Activo', '2023-02-16 10:59:00'),
(12, 'ISABEL', 'MARIA', 'ALVAREZ', 'PARRA', 'Estados Unidos', 'Cedula De Extrajeria', '545545JHH', 'isabel.cristina@cidenet.com.co', '2023-02-01', 'Infraestructura', 'Activo', '2023-02-26 12:28:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
